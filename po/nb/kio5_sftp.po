# Translation of kio6_sftp to Norwegian Bokmål
#
# Knut Yrvin <knut.yrvin@gmail.com>, 2002, 2003, 2005.
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2002, 2008, 2009, 2010, 2012, 2013, 2014.
# Jørgen Grønlund <jorgenhg@broadpark.no>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kio_sftp\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 00:46+0000\n"
"PO-Revision-Date: 2014-02-23 10:53+0100\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kio_sftp.cpp:223
#, kde-format
msgid "Incorrect or invalid passphrase"
msgstr "Ukorrekt eller ugyldig passord"

#: kio_sftp.cpp:272
#, kde-format
msgid "Could not allocate callbacks"
msgstr "Klarte ikke å tilordne tilbakekall"

#: kio_sftp.cpp:285
#, kde-format
msgid "Could not set log verbosity."
msgstr "Klarte ikke angi ordrikhet for loggen."

#: kio_sftp.cpp:290
#, kde-format
msgid "Could not set log userdata."
msgstr "Klarte ikke angi brukerdata for loggen."

#: kio_sftp.cpp:295
#, kde-format
msgid "Could not set log callback."
msgstr "Klarte ikke å tilordne tilbakekall for loggen."

#: kio_sftp.cpp:331 kio_sftp.cpp:333 kio_sftp.cpp:849
#, kde-format
msgid "SFTP Login"
msgstr "SFTP login"

#: kio_sftp.cpp:348
#, kde-format
msgid "Use the username input field to answer this question."
msgstr "Bruk tekstfeltet brukernavn til å svare på dette spørsmålet."

#: kio_sftp.cpp:360
#, kde-format
msgid "Please enter your password."
msgstr "Oppgi passordet ditt."

#: kio_sftp.cpp:365 kio_sftp.cpp:852
#, kde-format
msgid "Site:"
msgstr "Nettsted:"

#: kio_sftp.cpp:411
#, kde-format
msgctxt "error message. %1 is a path, %2 is a numeric error code"
msgid "Could not read link: %1 [%2]"
msgstr ""

#: kio_sftp.cpp:533
#, kde-format
msgid "Could not create a new SSH session."
msgstr "Klarte ikke opprette en ny SSH-økt."

#: kio_sftp.cpp:544 kio_sftp.cpp:548
#, kde-format
msgid "Could not set a timeout."
msgstr "Klarte ikke angi et tidsavbrudd."

#: kio_sftp.cpp:555
#, kde-format
msgid "Could not disable Nagle's Algorithm."
msgstr ""

#: kio_sftp.cpp:561 kio_sftp.cpp:566
#, kde-format
msgid "Could not set compression."
msgstr "Klarte ikke angi kompresjon."

#: kio_sftp.cpp:572
#, kde-format
msgid "Could not set host."
msgstr "Klarte ikke angi vert."

#: kio_sftp.cpp:578
#, kde-format
msgid "Could not set port."
msgstr "Klarte ikke angi port."

#: kio_sftp.cpp:586
#, kde-format
msgid "Could not set username."
msgstr "Klarte ikke angi brukernavn."

#: kio_sftp.cpp:593
#, kde-format
msgid "Could not parse the config file."
msgstr "Kunne ikke tolke oppsettsfila."

#: kio_sftp.cpp:610
#, kde-kuit-format
msgid "Opening SFTP connection to host %1:%2"
msgstr "Åpner SFTP-tilkobling til vert %1:%2"

#: kio_sftp.cpp:650
#, kde-format
msgid "Could not get server public key type name"
msgstr ""

#: kio_sftp.cpp:662
#, kde-format
msgid "Could not create hash from server public key"
msgstr "Klarte ikke å opprette hash ut fra tjenerens offentlige nøkkel"

#: kio_sftp.cpp:671
#, kde-format
msgid "Could not create fingerprint for server public key"
msgstr ""

#: kio_sftp.cpp:731
#, kde-format
msgid ""
"An %1 host key for this server was not found, but another type of key "
"exists.\n"
"An attacker might change the default server key to confuse your client into "
"thinking the key does not exist.\n"
"Please contact your system administrator.\n"
"%2"
msgstr ""

#: kio_sftp.cpp:748
#, kde-format
msgctxt "@title:window"
msgid "Host Identity Change"
msgstr ""

#: kio_sftp.cpp:750
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The host key for the server <emphasis>%1</emphasis> has changed.</"
"para><para>This could either mean that DNS spoofing is happening or the IP "
"address for the host and its host key have changed at the same time.</"
"para><para>The %2 key fingerprint sent by the remote host is:<bcode>%3</"
"bcode>Are you sure you want to continue connecting?</para>"
msgstr ""

#: kio_sftp.cpp:760
#, kde-format
msgctxt "@title:window"
msgid "Host Verification Failure"
msgstr ""

#: kio_sftp.cpp:762
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The authenticity of host <emphasis>%1</emphasis> cannot be established."
"</para><para>The %2 key fingerprint is:<bcode>%3</bcode>Are you sure you "
"want to continue connecting?</para>"
msgstr ""

#: kio_sftp.cpp:771
#, kde-format
msgctxt "@action:button"
msgid "Connect Anyway"
msgstr ""

#: kio_sftp.cpp:794 kio_sftp.cpp:813 kio_sftp.cpp:828 kio_sftp.cpp:841
#: kio_sftp.cpp:893 kio_sftp.cpp:903
#, kde-format
msgid "Authentication failed."
msgstr "Autentisering mislyktes."

#: kio_sftp.cpp:801
#, kde-format
msgid ""
"Authentication failed. The server didn't send any authentication methods"
msgstr ""
"Autentisering mislyktes. Tjeneren sendte ikke noen autentiseringsmetoder."

#: kio_sftp.cpp:850
#, kde-format
msgid "Please enter your username and password."
msgstr "Skriv inn brukernavn og passord."

#: kio_sftp.cpp:861
#, kde-format
msgid "Incorrect username or password"
msgstr "Ukorrekt brukernavn eller passord"

#: kio_sftp.cpp:910
#, kde-format
msgid ""
"Unable to request the SFTP subsystem. Make sure SFTP is enabled on the "
"server."
msgstr ""
"Klarer ikke forespørre SFTP-subsystemet. Se etter et SFTP er slått på på "
"tjeneren."

#: kio_sftp.cpp:915
#, kde-format
msgid "Could not initialize the SFTP session."
msgstr "Klarte ikke klargjøre SFTP-økta."

#: kio_sftp.cpp:919
#, kde-format
msgid "Successfully connected to %1"
msgstr "Vellykket tilkobling til %1"

#: kio_sftp.cpp:972
#, kde-format
msgid "Invalid sftp context"
msgstr ""

#: kio_sftp.cpp:1533
#, kde-format
msgid ""
"Could not change permissions for\n"
"%1"
msgstr ""
"Klarte ikke å forandre rettigheter for\n"
"%1"
