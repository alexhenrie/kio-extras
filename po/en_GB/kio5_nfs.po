# translation of kio_nfs.po to British English
# Copyright (C) 2004 Free Software Foundation, Inc.
#
# Andrew Coles <andrew_coles@yahoo.co.uk>, 2004.
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 00:46+0000\n"
"PO-Revision-Date: 2021-02-14 15:22+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.2\n"

#: kio_nfs.cpp:152
#, kde-format
msgid "Cannot find an NFS version that host '%1' supports"
msgstr "Cannot find an NFS version that host '%1' supports"

#: kio_nfs.cpp:322
#, kde-format
msgid "The NFS protocol requires a server host name."
msgstr "The NFS protocol requires a server host name."

#: kio_nfs.cpp:352
#, kde-format
msgid "Failed to initialise protocol"
msgstr "Failed to initialise protocol"

#: kio_nfs.cpp:820
#, kde-format
msgid "RPC error %1, %2"
msgstr "RPC error %1, %2"

#: kio_nfs.cpp:869
#, kde-format
msgid "Filename too long"
msgstr "File name too long"

#: kio_nfs.cpp:876
#, kde-format
msgid "Disk quota exceeded"
msgstr "Disk quota exceeded"

#: kio_nfs.cpp:882
#, kde-format
msgid "NFS error %1, %2"
msgstr "NFS error %1, %2"

#: nfsv2.cpp:391 nfsv2.cpp:442 nfsv3.cpp:429 nfsv3.cpp:573
#, kde-format
msgid "Unknown target"
msgstr "Unknown target"

#~ msgid "%1: Unsupported NFS version"
#~ msgstr "%1: Unsupported NFS version"

#~ msgid "No space left on device"
#~ msgstr "No space left on device"

#~ msgid "Read only file system"
#~ msgstr "Read only file system"

#~ msgid "Failed to mount %1"
#~ msgstr "Failed to mount %1"

#~ msgid "An RPC error occurred."
#~ msgstr "An RPC error occurred."
